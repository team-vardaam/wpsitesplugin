<?php
   /*
   Plugin Name: WP Sites Manager
   description: A new start to make awesome application by Vardaam
  a plugin to create awesomeness and spread joy
   Version: 1.0
   Author: Aakanksha
   License: GPL2
   */

////////////////////////////////////////////////////
/* START OF WORKING CODE */
////////////////////////////////////////////////////

global $wpdb;
// Posts
function post_list( $data ) {
    $posts = get_posts();
   
    if ( empty( $posts ) ) {
      return null;
    }
   
    return $posts;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp_sites_manager/v1', '/posts/', array(
      'methods' => 'GET',
      'callback' => 'post_list',
    ) );
  } );


// plugins
function plugin_list(){

  // Check if get_plugins() function exists. This is required on the front end of the
  // site, since it is in a file that is normally only loaded in the admin.
  if ( ! function_exists( 'get_plugins' ) ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
  }

  $plugins = get_plugins();

  $items = array();
  foreach($plugins as $key=>$p){
    if(isset($p)){
      if(is_plugin_active($key)){
          $p['Status'] = "Active";
          $items[] = $p;
      }
      else {
          $p['Status'] = "InActive";
          $items[] = $p;
      }
    }
  }
  return $items;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp_sites_manager/v1', '/plugins/', array(
      'methods' => 'GET',
      'callback' => 'plugin_list',
    ) );
  } );

// Active Theme
function active_theme(){
    $theme = wp_get_theme();
    $theme_Detail = array(
            'Name' => $theme->get( 'Name' ),
            'ThemeURI' => $theme->get( 'ThemeURI' ),
            'Description' => $theme->get( 'Description' ),
            'Author' => $theme->get( 'Author' ),
            'AuthorURI' => $theme->get( 'AuthorURI' ),
            'Version' => $theme->get( 'Version' ),
            'Template' => $theme->get( 'Template' ),
            'Status' => $theme->get( 'Status' ),
            'Tags' => $theme->get( 'Tags' ),
            'TextDomain' => $theme->get( 'TextDomain' ),
            'DomainPath' => $theme->get( 'DomainPath' ),
            'RequiresWP' => $theme->get( 'RequiresWP' ),
            'RequiresPHP' => $theme->get( 'RequiresPHP' ),
    );
    return $theme_Detail;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp_sites_manager/v1', '/active_theme/', array(
      'methods' => 'GET',
      'callback' => 'active_theme',
    ) );
  } );


// Site Details
function site_details(){
  
  $site_title = get_bloginfo( 'name' );
  $site_url = network_site_url( '/' );
  $site_description = get_bloginfo( 'description' );

  $from_api = get_site_transient( 'update_core' );
  $updates = $from_api->updates;  

  $user_ID = 0;

  $theme_Detail = array(
          'site_title' => $site_title,
          'site_url' => $site_url,
          'site_description' => $site_description,
          'current_version' => get_bloginfo( 'version' ),
          'latest_version' => $updates[0]->version,
          'user_id' => $user_ID
  );
  return $theme_Detail;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/site_details/', array(
    'methods' => 'GET',
    'callback' => 'site_details',
  ) );
} );


/**
 * Handles resetting the user's password.
 *
 * @since 2.5.0
 *
 * @param WP_User $user     The user
 * @param string  $new_pass New password for the user in plaintext
 */
function reset_password_through_api( $user) {

  $json = file_get_contents('php://input');
  $obj = json_decode($json);
  $password = $obj->password;

	wp_set_password($password, $user['id'] );
	update_user_option( $user['id'], 'default_password_nag', false, true );

}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/reset_password/(?P<id>\d+)', array(
    'methods' => 'POST',
    'callback' => 'reset_password_through_api',
  ) );
} );

// All Users
function users_list(){
  $users = get_users();
  return $users;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/all_users/', array(
    'methods' => 'GET',
    'callback' => 'users_list',
  ) );
} );

// Get user role
function get_user_role($user){
  $user_meta=get_userdata($user['id']);
  $user_roles=$user_meta->roles; 
  return $user_roles[0];
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/get_user_role/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'get_user_role',
  ) );
} );

// Get User
function get_user($user){
  $user = get_userdata($user['id']);
  return $user;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/user/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'get_user',
  ) );
} );

// Delete the user
function delete_user($user){
  require_once ABSPATH . 'wp-admin/includes/user.php';
  $json = file_get_contents('php://input');
  $obj = json_decode($json);
  $id = $obj->id;

 $response =  wp_delete_user($id, null);
 return $response;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/delete_user', array(
    'methods' => 'POST',
    'callback' => 'delete_user',
  ) );
} );

// Spam comments count
function get_spam_comments_count(){
  $count = wp_count_comments();
  return $count->spam;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/count_spam_comments', array(
    'methods' => 'GET',
    'callback' => 'get_spam_comments_count',
  ) );
} );

// Comments List
function get_comments_list(){
  require_once ABSPATH . 'wp-includes/comment.php';
  $comments = get_comments();
  return $comments;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/get_comments', array(
    'methods' => 'GET',
    'callback' => 'get_comments_list',
  ) );  
} );

// Empty Trash
function empty_trash_comments(){
  global $wpdb;
  $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'trash'");
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/empty_trash_comments', array(
    'methods' => 'POST',
    'callback' => 'empty_trash_comments',
  ) );
} );

// Trash spam comments
function trash_spam_comments(){
  global $wpdb;
  $wpdb->query("UPDATE $wpdb->comments SET comment_approved = 'trash' WHERE comment_approved = 'spam'");

}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/trash_spam_comments', array(
    'methods' => 'POST',
    'callback' => 'trash_spam_comments',
  ) );
} );

// Empty spam comments
function empty_spam_comments(){
  global $wpdb;
  $response = $wpdb->query("DELETE FROM $wpdb->comments WHERE comment_approved = 'spam'");

}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/empty_spam_comments', array(
    'methods' => 'POST',
    'callback' => 'empty_spam_comments',
  ) );
} );

// Get comment status by comment id
function get_comment_status_by_Id($user){
  require_once ABSPATH . 'wp-includes/comment.php';
  $status = wp_get_comment_status($user['id']);
  return $status;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/get_comment_status/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'get_comment_status_by_Id',
  ) );
} );

////////////////////////////////////////////////////
// FORM DATA 
////////////////////////////////////////////////////

function get_all_forms() {
		
  $form_information = [];

  $Gravity_Form_url = 'gravityforms/gravityforms.php';
  $Contact_Form7 ='contact-form-7/wp-contact-form-7.php';
  $whoocommerce_url ='woocommerce/woocommerce.php';

      if( is_plugin_active( $Gravity_Form_url ) ){
    $forms = GFAPI::get_forms();
          $Gravity_Form_data = [];	
          foreach ( $forms as $form) {
              $notifications = $form['notifications'];
              $notify = [];
              $admin_email = get_option( 'admin_email' );
              foreach ($notifications as $notification){
                  if($notification['to'] == '{admin_email}'){
                      $notification['to'] = $admin_email;
                  }
                  $notify[] = [
                      'name' => $notification['name'],
                      'to' => $notification['to'],
                      'bcc' => $notification['bcc'],
                      'subject' => $notification['subject'],
                      'admin-email' => $admin_email,
                  ];
              }
      
      $fields = $form['fields'];
      $captcha = false;
      if (!empty($fields)) {
        foreach($fields as $field){
          $captcha = ($field['type'] == 'captcha') ? true : false;
        }	
      }

              $Gravity_Form_data[] = [
                  'form-title' => $form['title'],
        'captcha' => $captcha,
                  'notifications' =>  $notify,
              ];
          }
    $form_information['gf-data'] = $Gravity_Form_data;
      }
  
// --------------------- CONTACT FORM7 DATA ------------------------------
  
  
  if ( is_plugin_active( $Contact_Form7 ) ) {
    $args = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
          $cf7Forms = get_posts( $args );
          $cf7data = [];
          foreach ($cf7Forms as $cf7Form)
          {			
              $meta_datas = get_post_meta($cf7Form->ID);
              $notifications = [];
              foreach($meta_datas as $k => $meta_data) {					
                  if (strpos($k, 'mail') !== false && isset($meta_data[0])) {
                      $meta_data = unserialize($meta_data[0]);
                      if (!empty($meta_data['recipient']) && !empty($meta_data['active']) ) {
                          $notifications[] = [
                              'subject' => $meta_data['subject'],
                              'to' => $meta_data['recipient']
                          ];
                      }
                  }
              }
              $cf7data['forms'] = [
                  'form-title' => $cf7Form->post_title,
                  'notifications' => $notifications
              ];
          }
    $wp7_captcha = get_option( 'wpcf7' );
    $wp7_recaptcha = (!empty($a['recaptcha'])) ? true : false;
    
    $cf7data['global'] = [
      'recaptcha' => $wp7_recaptcha,
    ];
    
    $form_information['cf7-data'] = $cf7data;
  }
  
// --------------------------- Woocommerce Data ---------------------------

  if( is_plugin_active( $whoocommerce_url ) ){
    $get_wc_mail = WC()->mailer()->get_emails();
    $wc_data = [];
    foreach($get_wc_mail as $wc_mail_data)
    {
      $notifications[0] = [
        'to' => $wc_mail_data->recipient,
      ];
      $wc_data[] = [
        'form-title' =>  $wc_mail_data->title,
        'notifications' => $notifications,
      ];
    }
    $form_information['wc-data'] = $wc_data;
  }
  return $form_information;
}


add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_sites_manager/v1', '/get_all_forms_data', array(
    'methods' => 'GET',
    'callback' => 'get_all_forms',
  ) );
} );
  
////////////////////////////////////////////////////
/* END OF WORKING CODE */
////////////////////////////////////////////////////


?>